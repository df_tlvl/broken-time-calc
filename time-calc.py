
def time_calc(input_str):
    result = 0
    operation = detect_operation(input_str)
    operands = get_operands(input_str, operation)
    for op in operands:
        op = convert_to_seconds(op)
    result = compute(operands, operation)
    #result = convert_to_time_str(result)
    return result

def detect_operation(input_str):
    if '+' in input_str:
        return '+'
    if '-' in input_str:
        return '-'
    return None

def get_operands(input_str, operation):
    operands = input_str.split(operation)   
    for op in operands:
        op = op.strip()
    return operands

def convert_to_seconds(time_str):
    mins, secs = time_str.split('.')
    mins = int(mins)
    secs = int(secs)
    return secs + mins * 60

def compute(operands, operation):
    if operation is '+':
        return operands[0] + operands[1]
    if operation is '-':
        return operands[0] - operands[1]
    return None

def convert_to_time_str(secs):
    mins = secs // 60 # целочисленное деление (в python3!!!)
    secs %= 60 # остаток от деления seconds = seconds % 60
    return str(mins) + '.' + str(secs)
    

print(time_calc('3.40 + 1.00'))